# Vita Boilerplate
Some small boilerplate code for vitasdk development.

Licensed under the Unlicense so anyone can use it as they please.

## What to change if you are using this

Change `project(vita_boilerplate)` to `project(YOUR-HOMEBREW-HERE)`

Change `3O6K00000` to another title ID where the first 4 digits are recommended to be unique to you and the last 5 are recommended to be unique to the application (PSA: DON'T USE ABCD12345)

Change `Vita Boilerplate` to the name of your homebrew application

Change `12.34` to the version of your application
